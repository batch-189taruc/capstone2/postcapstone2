const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const port = 4001;
const cors = require('cors');
const app = express();
const userRoutes = require('./routes/userRoutes.js')
const productRoutes = require('./routes/productRoutes.js')


dotenv.config();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/spices", productRoutes);


mongoose.connect(`mongodb+srv://zuittdiscussion:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.xbtkvqt.mongodb.net/Capstone2?retryWrites=true&w=majority`, {
	useNewUrlParser : true,
	useUnifiedTopology : true
	}
)


let db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'error'));
db.on('open', () => console.log('Now connected to MongoDB Atlas!'))



app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
})

