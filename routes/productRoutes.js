const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController.js');
const auth = require("../auth.js")


//add product as admin

router.post("/addproduct", auth.verify, (req, res) => {
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){

	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send("Unauthorized action")
	}

})

//show all active products
router.get("/", (req,res) => {
	productController.showAllActive().then(resultFromController => res.send(resultFromController))
});

//show 1 product
router.get("/:productId", (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})



//archive a product

router.put('/archive/:productId', auth.verify, (req, res) => {
const userData = auth.decode(req.headers.authorization)


	let data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
})

//unarchive a product

router.put('/unarchive/:productId', auth.verify, (req, res) => {
const userData = auth.decode(req.headers.authorization)


	let data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin
	}

	productController.unArchiveProduct(data).then(resultFromController => res.send(resultFromController))
})

//delete product

router.delete("/deleteproduct/:productId", auth.verify, (req,res) => {
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){
	productController.deleteProduct(req.params).then(resultFromController => res.send(resultFromController))
	}

})

//updating a product

router.put("/updateproduct/:productId", auth.verify, (req,res) => {
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}

})


//add product as admin

router.post("/addproduct", auth.verify, (req, res) => {
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){

	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	}

})

//show 1 product
router.post("/checkproduct", (req,res) => {
	productController.checkProduct(req.body).then(resultFromController => res.send(resultFromController))
})


//show placedorder per product
router.get("/vieworders/:productId", auth.verify, (req, res) =>{
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin
if(isAdminData == true){
		productController.viewOrders(req.params.productId).then(resultFromController => res.send(resultFromController))
	}


})

module.exports = router;