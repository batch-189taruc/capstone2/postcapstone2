const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController.js');
const auth = require("../auth.js")
const Product = require('../models/Product.js');



//register

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

//login

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


//set admin as admin

router.put("/setadmin/:id", auth.verify, (req, res) => {
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){

	userController.updateAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send("Unauthorized action")
	}

})

//Place an order
router.post("/placeorder", auth.verify, (req, res) => {
const userData = auth.decode(req.headers.authorization);
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin


	let data = {
		userId: userData.id,
		username: userData.username,
		productName: req.body.name,
		quantity: req.body.quantity,
		price: req.body.price,
		subtotal: req.body.subtotal
	}

	if(isAdminData == true){
		res.send("Registered user function only")
	}else{

	userController.addorder(data, userData).then(resultFromController => res.send(resultFromController))}

});

//Retrieve user orders
router.get("/myorders", auth.verify, (req, res) =>{
const userData = auth.decode(req.headers.authorization);
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin
let data = {userId: userData.id}
if(isAdminData == true){
		res.send("Registered user function only")
	}else{

	userController.getOrders({id: userData.id}).then(resultFromController => res.send(resultFromController))}


})




//delete codes, delete own acct

router.delete("/deletemyaccount", auth.verify, (req,res) =>{
const userData = auth.decode(req.headers.authorization);
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin
if(isAdminData == true){
		res.send("Registered user function only")
	} else{
		if(userData.email == req.body.email){
		userController.deleteMyAccount(req.body).then(resultFromController => res.send("Account has been deleted"))}
		else{
			res.send("Unauthorized action")
		}
	}


})

//delete an admin acct
router.delete("/deleteadmin", auth.verify, (req,res) =>{
const userData = auth.decode(req.headers.authorization);
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData == true){
		userController.deleteAdmin(req.body).then(resultFromController => res.send("Admin account has been deleted"))

	} else{
		res.send("Admin function only")
	}


})


//update own user info (only for email, contact, and address)

router.put("/updatemyinfo", auth.verify, (req,res) => {
const userData = auth.decode(req.headers.authorization);
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){
	res.send("Unauthorized action. You're not the user")
	}else{
		
		userController.updateMyInfo(userData, req.body).then(resultFromController => res.send(resultFromController))
	}

})
//get 1 user detail
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController));
})
//checkemail only
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})
//retrieve all users as admin
router.get("/allusers", auth.verify, (req, res) =>{
const userData = auth.decode(req.headers.authorization);
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData === true){
		userController.getAllUsers().then(resultFromController => res.send(resultFromController))
		
	}
	

})


//retrieve all spices as admin
router.get("/allspices", auth.verify, (req, res) =>{
const userData = auth.decode(req.headers.authorization);
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData === true){
		userController.getAllSpices().then(resultFromController => res.send(resultFromController))
		
	}
	

})

//retrieve 1 user as an admin
router.get("/allusers/:userId", (req,res) => {
const userData = auth.decode(req.headers.authorization);
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData === true){
		userController.adminGetUser(req.params).then(resultFromController => res.send(resultFromController))
		
	}
	
})

//revrieve 1 product as an admin
router.get("/allspices/:productId", (req,res) => {
const userData = auth.decode(req.headers.authorization);
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData === true){
		userController.adminGetProduct(req.params).then(resultFromController => res.send(resultFromController))
		
	}
	
})

//delete user as an admin
router.delete("/deleteuser/:userId", (req,res) =>{
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){
	userController.deleteUser(req.params.userId).then(resultFromController => res.send(resultFromController))
	}
})


module.exports = router;

//update user as an admin
router.put("/updateuser1/:userId", (req,res) =>{
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){
	userController.updateUser1(req.params).then(resultFromController => res.send(resultFromController))
	}
})
//remove user as an admin
router.put("/updateuser2/:userId", (req,res) =>{
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){
	userController.updateUser2(req.params).then(resultFromController => res.send(resultFromController))
	}
})

// view all placedOrders isPaid false
router.get("/viewallorders", (req,res) =>{
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin

if(isAdminData){
	userController.viewAllOrders().then(resultFromController => res.send(resultFromController))
	}
})


//delete 1 productOrders
router.get("/deletespicecart", (req,res) =>{
const userData = auth.decode(req.headers.authorization)
const confirmAdmin = auth.decode(req.headers.authorization);
const isAdminData = confirmAdmin.isAdmin
	let data = {
		userId: userData.id}

if(isAdminData === false){
	userController.deleteSpiceCart(data).then(resultFromController => res.send(resultFromController))
}
})

module.exports = router;



